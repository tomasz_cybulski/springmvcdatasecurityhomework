INSERT INTO PUBLIC.USER(ID, USERNAME, PASSWORD) VALUES
('1', 'janusz@wp.pl', 'janusz01'),
('2', 'zbyszek@gmial.com', 'zbyszek01'),
('3', 'marian@o2.pl', 'marian01');

INSERT INTO PUBLIC .USER_ORDER(ID, CREATIONDATE, USER) VALUES
('1', '2018', (SELECT ID FROM USER WHERE ID='1')),
('2', '2018', (SELECT ID FROM USER WHERE ID='2')),
('3', '2018', (SELECT ID FROM USER WHERE ID='3'));

INSERT INTO PUBLIC .USER_ORDER_ITEMS(USER_ORDER_ID, ITEMS) VALUES
('1', '1'),
('1', '2'),
('1', '5'),
('2', '3'),
('2', '6'),
('2', '9'),
('3', '2'),
('3', '7'),
('3', '4');