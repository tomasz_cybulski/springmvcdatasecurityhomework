package pl.codementors.homeworks.spring.springHomework.repository;

import org.hibernate.criterion.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.codementors.homeworks.spring.springHomework.model.User;
import pl.codementors.homeworks.spring.springHomework.model.UserOrder;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserOrderRepository extends CrudRepository<UserOrder, String> {

    List<UserOrder> findAll();

    List<UserOrder> findByUser(User userid);

    UserOrder findByIdAndUser(String orderid, User userid);

    UserOrder findById(String id);

    void delete(UserOrder userOrder);
}
