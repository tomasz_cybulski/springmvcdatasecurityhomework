package pl.codementors.homeworks.spring.springHomework.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "UserOrder")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @NotEmpty
    @ElementCollection(targetClass = String.class)
    private List<String> items;

    private String creationdate ;

    @ManyToOne
    @JoinColumn(name = "user", referencedColumnName = "id")
//    @JsonManagedReference
    private User user;

    public UserOrder(){}

    public UserOrder(List<String> items, String creationdate, User user) {
        this.items = items;
        this.creationdate = creationdate;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public String getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserOrder{" +
                "id=" + id +
                ", items=" + items +
                ", creationdate=" + creationdate +
                ", user=" + user +
                '}';
    }
}
