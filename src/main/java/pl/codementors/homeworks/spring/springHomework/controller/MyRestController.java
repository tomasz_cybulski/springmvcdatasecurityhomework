package pl.codementors.homeworks.spring.springHomework.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.codementors.homeworks.spring.springHomework.model.User;
import pl.codementors.homeworks.spring.springHomework.model.UserOrder;
import pl.codementors.homeworks.spring.springHomework.repository.UserOrderRepository;
import pl.codementors.homeworks.spring.springHomework.repository.UserRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class MyRestController {

    private UserOrderRepository userOrderRepository;
    private UserRepository userRepository;

    private static final Logger LOG = LoggerFactory.getLogger(MyRestController.class);


    @Autowired
    public MyRestController(UserOrderRepository userOrderRepository, UserRepository userRepository) {
        this.userOrderRepository = userOrderRepository;
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/orders")
    public ResponseEntity<UserOrder> allOrders() {
        if (userOrderRepository.findAll() != null) {
            return new ResponseEntity( userOrderRepository.findAll(), HttpStatus.OK);
        } else {
            return new ResponseEntity("Orders not found!", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/user")
    public ResponseEntity<User> getUserFromAuthentication(Authentication authentication) {
        String currentUsername = authentication.getName();
        if (userRepository.findByUsername(currentUsername) != null) {
            User user = userRepository.findByUsername(currentUsername).get();
            return new ResponseEntity( user, HttpStatus.OK);
        } else {
            return new ResponseEntity("User not found!", HttpStatus.NOT_FOUND);
        }
    }

//    @GetMapping(value = "/user/{userid}")
//    public ResponseEntity<User> userFindById(@PathVariable(value = "userid") String userid) {
//
//        if (userRepository.findById(userid) != null) {
//            return new ResponseEntity( userRepository.findById(userid), HttpStatus.OK);
//        } else {
//            return new ResponseEntity("User not found!", HttpStatus.NOT_FOUND);
//        }
//    }

    @GetMapping(value = "/user/orders")
    public ResponseEntity<UserOrder> userOrders (Authentication authentication){
        User currentUser = getUserFromAuthentication(authentication).getBody();
        LOG.info(currentUser.toString());
        if (userOrderRepository.findByUser(currentUser).isEmpty()) {
            return new ResponseEntity("Orders not found!", HttpStatus.NOT_FOUND);
        }  else {
            return new ResponseEntity( userOrderRepository.findByUser(currentUser), HttpStatus.OK);
        }
    }

//    @GetMapping(value = "/user/{userid}/orders")
//    public ResponseEntity<UserOrder> userOrders (@PathVariable(value = "userid") String userid){
//        User userFindByID = userRepository.findById(userid);
//        if (userOrderRepository.findByUser(userFindByID).isEmpty()) {
//            return new ResponseEntity("Orders not found!", HttpStatus.NOT_FOUND);
//        }  else {
//            return new ResponseEntity( userOrderRepository.findByUser(userFindByID), HttpStatus.OK);
//        }
//    }

    @PostMapping(value = "/user/add")
    @ResponseBody
    public ResponseEntity<UserOrder> addUserOrder(Authentication authentication, @RequestBody @Valid UserOrder userOrder) {
        User currentUser = getUserFromAuthentication(authentication).getBody();
        userOrder.setUser(currentUser);
        userOrderRepository.save(userOrder);
        return new ResponseEntity( userOrder, HttpStatus.OK);
    }


//    @PostMapping(value = "/user/{userid}/add")
//    @ResponseBody
//    public ResponseEntity<UserOrder> addUserOrder(@PathVariable("userid") String userid, @RequestBody @Valid UserOrder userOrder) {
////        LOG.info(userOrder.toString());
//        if (userRepository.findById(userid) != null) {
//            userOrderRepository.save(userOrder);
//            return new ResponseEntity( userOrder, HttpStatus.OK);
//        } else {
//            return new ResponseEntity("User not found!", HttpStatus.NOT_FOUND);
//        }
//    }

    @DeleteMapping("/user/delete/{orderid}")
    @ResponseBody
    public  ResponseEntity<UserOrder> deleteUser(Authentication authentication,
                                                 @PathVariable("orderid") String orderid) {
        User currentUser = getUserFromAuthentication(authentication).getBody();
        if (userOrderRepository.findByIdAndUser(orderid, currentUser) != null) {
            UserOrder orderToDelete = userOrderRepository.findByIdAndUser(orderid, currentUser);
            userOrderRepository.delete(orderToDelete);
            return new ResponseEntity( orderToDelete, HttpStatus.OK);
        } else {
            return new ResponseEntity("Order not found!", HttpStatus.NOT_FOUND);
        }
    }

//    @DeleteMapping("/user/{userid}/delete/{orderid}")
//    public  ResponseEntity<UserOrder> deleteUser(@PathVariable("userid") String userid,
//                                                 @PathVariable("orderid") String orderid) {
//        User user = userRepository.findById(userid);
////        LOG.info(userOrderRepository.findByIdAndUser(orderid, user);
//        if (userOrderRepository.findByIdAndUser(orderid, user) != null) {
//            userOrderRepository.delete(userOrderRepository.findByIdAndUser(orderid, user));
//            return new ResponseEntity( userOrderRepository.findByIdAndUser(orderid, user), HttpStatus.OK);
//        } else {
//            return new ResponseEntity("Order not found!", HttpStatus.NOT_FOUND);
//        }
//    }

    @PutMapping("user/edit/{orderid}")
    public ResponseEntity<UserOrder> editOrder(Authentication authentication,
                                               @PathVariable("orderid") String orderid,
                                               @RequestBody @Valid UserOrder userOrder) {
        User currentUser = getUserFromAuthentication(authentication).getBody();
        if (userOrderRepository.findByIdAndUser(orderid, currentUser) != null) {
            UserOrder userOrderToEdit = userOrderRepository.findByIdAndUser(orderid, currentUser);
            List<String> editedItems = userOrder.getItems();
            userOrderToEdit.setItems(editedItems);
            String editedCreationdate = userOrder.getCreationdate();
            userOrderToEdit.setCreationdate(editedCreationdate);
            userOrderRepository.save(userOrderToEdit);
            return new ResponseEntity( userOrderToEdit, HttpStatus.OK);
        } else {
            return new ResponseEntity("Order not found!", HttpStatus.NOT_FOUND);
        }
    }

//    @PutMapping("user/{userid}/edit/{orderid}")
//    public ResponseEntity<UserOrder> editOrder(@PathVariable("userid") String userid,
//                               @PathVariable("orderid") String orderid,
//                               @RequestBody @Valid UserOrder userOrder) {
//        User user = userRepository.findById(userid);
//        if (userOrderRepository.findByIdAndUser(orderid, user) != null) {
//            UserOrder userOrderToEdit = userOrderRepository.findByIdAndUser(orderid, user);
//            List<String> editeItems = userOrder.getItems();
//            userOrderToEdit.setItems(editeItems);
//            userOrderRepository.save(userOrderToEdit);
//            return new ResponseEntity( userOrderToEdit, HttpStatus.OK);
//        } else {
//            return new ResponseEntity("Order not found!", HttpStatus.NOT_FOUND);
//        }
//    }
}
