package pl.codementors.homeworks.spring.springHomework.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.codementors.homeworks.spring.springHomework.model.User;
import pl.codementors.homeworks.spring.springHomework.repository.UserRepository;


import java.util.Arrays;
import java.util.Optional;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private UserRepository repository;

    @Autowired
    public SecurityConfiguration(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/orders").permitAll()
                .anyRequest().authenticated()
//                .antMatchers("/user/**").authenticated()
//                .antMatchers("/user/order").authenticated()
                .and().csrf().disable()
                .httpBasic();
    }

    public UserDetailsService userDetailsService(){
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
                final Optional<User> user = repository.findByUsername(username);
                return user.map(u -> new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))))
                        .orElseThrow(()-> new UsernameNotFoundException(username));

            }
        };
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        auth.authenticationProvider(authenticationProvider);
    }
}

