package pl.codementors.homeworks.spring.springHomework.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.codementors.homeworks.spring.springHomework.model.User;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User findById(String userid);

    Optional<User> findByUsername(String username);

}
